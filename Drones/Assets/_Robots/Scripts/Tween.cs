﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tween : MonoBehaviour
{
    #region Enums
    public enum Space
    {
        World,
        Local,
        Canvas
    }

    public enum State
    {
        Play,
        Pause,
        Stop
    }
    #endregion

    #region Classes
    public class TweenModule<T>
    {
        public T From;
        public T To;

        public bool IsTweenable => !From.Equals(To);

        private bool _isReversed = false;

        public void SetValue(T from, T to)
        {
            From = from;
            To = to;
        }

        public void Reverse()
        {
            if (!_isReversed)
            {
                _isReversed = true;
                Invert();
            }
        }

        public void Normalize()
        {
            if (_isReversed)
            {
                _isReversed = false;
                Invert();
            }
        }

        private void Invert()
        {
            T from = From;
            T to = To;
            From = to;
            To = from;
        }
    }
    [Serializable] public class TweenVectorModule : TweenModule<Vector3>
    {
        [Space]
        public Space Space;

        public void SetValue(Vector3 from, Vector3 to, Space space)
        {
            base.SetValue(from, to);
            Space = space;
        }
    }
    [Serializable] public class TweenFloatModule : TweenModule<float>
    {
        [Serializable]
        public class TweenFloatEvent : UnityEvent<float> { }

        [Space]
        public TweenFloatEvent OnGetFloatValue;
    }
    [Serializable] public class TweenBase
    {
        public bool PlayOnStart;
        public bool PlayOnEnable;
        public float Duration;
    }
    [Serializable] public class TweenEvent : UnityEvent<Tween> { }
    #endregion

    [Header("Base")]
    public TweenBase Main;

    [Header("Modules")]
    public TweenVectorModule Position;
    public TweenVectorModule Rotation;
    public TweenVectorModule Scale;
    public TweenFloatModule Float;

    [Header("Events")]
    public TweenEvent OnStarted;
    public TweenEvent OnCompleted;
    [Header("Play Events")]
    public TweenEvent OnPlay;
    public TweenEvent OnPlayCompleted;
    [Header("Reverse Events")]
    public TweenEvent OnReverse;
    public TweenEvent OnReverseCompleted;

    public bool IsPlaying { get { return _state == State.Play; } }
    public float FloatValue { get; set; }

    private Transform _target;
    private State _state;
    private bool _reversing;
    private float _timer;
    private float _timerStep;

    void Awake()
    {
        _target = transform;
        _state = State.Stop;
    }

    void Start()
    {
        if (Main.PlayOnStart)
            Play();
    }

    void OnEnabled()
    {
        if (Main.PlayOnEnable)
            Play();
    }

    #region Tween Functions
    public static Tween AttachTo(GameObject target)
    {
        return target.AddComponent<Tween>().Initialize();
    }

    private Tween Initialize()
    {
        Main = new TweenBase();
        Position = new TweenVectorModule();
        Rotation = new TweenVectorModule();
        Scale = new TweenVectorModule();
        Float = new TweenFloatModule();
        OnStarted = new TweenEvent();
        OnPlay = new TweenEvent();
        OnReverse = new TweenEvent();
        OnCompleted = new TweenEvent();
        OnPlayCompleted = new TweenEvent();
        OnReverseCompleted = new TweenEvent();
        return this;
    }

    public Tween SetDuration(float seconds)
    {
        Main.Duration = seconds;
        return this;
    }

    public Tween SetPosition(Vector3 from, Vector3 to, Space space = Space.World)
    {
        Position.SetValue(from, to, space);
        return this;
    }

    public Tween SetRotation(Vector3 from, Vector3 to, Space space = Space.World)
    {
        Rotation.SetValue(from, to, space);
        return this;
    }

    public Tween SetScale(Vector3 from, Vector3 to)
    {
        Scale.SetValue(from, to);
        return this;
    }

    public Tween SetFloat(float from, float to)
    {
        Float.SetValue(from, to);
        return this;
    }
    #endregion

    #region Control Functions

    public void Play()
    {
        _state = State.Play;
        _timer = 0;

        NormalizeModules();

        if (OnStarted != null)
            OnStarted.Invoke(this);

        if (OnPlay != null)
            OnPlay.Invoke(this);
    }

    public void Reverse()
    {
        _state = State.Play;
        _timer = 0;
        _timerStep = 0;

        ReverseModules();

        if (OnStarted != null)
            OnStarted.Invoke(this);

        if (OnReverse != null)
            OnReverse.Invoke(this);
    }

    public void Pause(float duration = 0)
    {
        _state = State.Pause;

        if (duration > 0)
            Invoke("Resume", duration);
    }

    public void Stop()
    {
        _state = State.Stop;
        _timer = 0;
        _timerStep = 0;
    }

    public void Resume()
    {
        if(_state == State.Pause)
            _state = State.Play;
    }

    #endregion

    #region Update Functions

    void FixedUpdate()
    {
        if (_state == State.Pause || _state == State.Stop)
        {
            return;
        }

        if (_state == State.Play)
        {
            _timer += Time.deltaTime;
            _timerStep = Mathf.InverseLerp(0, Main.Duration, _timer);

            if (Position.IsTweenable)
                UpdatePosition();

            if (Rotation.IsTweenable)
                UpdateRotation();

            if (Scale.IsTweenable)
                UpdateScale();

            if (Float.IsTweenable)
                UpdateFloat();

            if (_timer >= Main.Duration)
            {
                _timer = 0;
                _timerStep = 0;
                _state = State.Stop;

                if (OnCompleted != null)
                    OnCompleted.Invoke(this);

                if (!_reversing && OnPlayCompleted != null)
                    OnPlayCompleted.Invoke(this);

                if (_reversing && OnReverseCompleted != null)
                    OnReverseCompleted.Invoke(this);

                return;
            }
        }
    }

    private void UpdatePosition()
    {
        switch (Position.Space)
        {
            case Space.World:
                _target.position = Vector3.Lerp(Position.From, Position.To, _timerStep);
                break;
            case Space.Local:
                _target.localPosition = Vector3.Lerp(Position.From, Position.To, _timerStep);
                break;
            case Space.Canvas:
                ((RectTransform)_target).anchoredPosition = Vector3.Lerp(Position.From, Position.To, _timerStep);
                break;
        }
    }

    private void UpdateRotation()
    {
        switch (Rotation.Space)
        {
            case Space.World:
                _target.eulerAngles = Vector3.Lerp(Rotation.From, Rotation.To, _timerStep);
                break;
            case Space.Local:
                _target.localEulerAngles = Vector3.Lerp(Rotation.From, Rotation.To, _timerStep);
                break;
            case Space.Canvas:
                _target.eulerAngles = Vector3.Lerp(Rotation.From, Rotation.To, _timerStep);
                break;
        }
    }

    private void UpdateScale()
    {
        _target.localScale = Vector3.Lerp(Scale.From, Scale.To, _timerStep);
    }

    private void UpdateFloat()
    {
        FloatValue = Mathf.Lerp(Float.From, Float.To, _timerStep);

        if (Float.OnGetFloatValue != null)
            Float.OnGetFloatValue.Invoke(FloatValue);
    }

    private void ReverseModules()
    {
        _reversing = true;

        Position.Reverse();
        Rotation.Reverse();
        Scale.Reverse();
        Float.Reverse();
    }

    private void NormalizeModules()
    {
        _reversing = false;

        Position.Normalize();
        Rotation.Normalize();
        Scale.Normalize();
        Float.Normalize();
    }

    #endregion
}
