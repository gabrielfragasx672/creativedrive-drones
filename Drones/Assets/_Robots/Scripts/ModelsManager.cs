﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[Serializable]
public class JsonModelCollection
{
    public List<JsonModel> models;
}

[Serializable]
public class JsonModel
{
    public string name;
    public float[] position;
    public float[] rotation;
    public float[] scale;
    public int index;
    public int colorOption;
    public int textureOption;

    public Vector3 Convert(float[] values)
    {
        return new Vector3(values[0], values[1], values[2]);
    }

    public float[] Convert(Vector3 vector)
    {
        return new float[] { vector.x, vector.y, vector.z };
    }

    public static implicit operator Model(JsonModel jsonModel)
    {
        Model model = new Model();
        model.Name = jsonModel.name;
        model.Position = jsonModel.Convert(jsonModel.position);
        model.Rotation = jsonModel.Convert(jsonModel.rotation);
        model.Scale = jsonModel.Convert(jsonModel.scale);
        model.Index = jsonModel.index;
        model.ColorOption = jsonModel.colorOption;
        model.TextureOption = jsonModel.textureOption;

        return model;
    }
}

public class ModelsManager : MonoBehaviour
{
    [Serializable]
    public struct ResourceFolder
    {
        public string Folder;
        public string Prefab;
        public string Icon;
        public string[] Textures;
        public string[] TextureIcons;
        public Color[] Colors;
    }

    [Serializable]
    public struct ResourceModel
    {
        public GameObject Object;
        public Sprite Icon;
        public Texture[] Textures;
        public Sprite[] TextureIcons;
        public Color[] Colors;
    }

    public class ModelEvent : UnityEvent<Model> { }

    public ResourceFolder[] ResourceFolders;
    public List<ResourceModel> ResourceModels;
    public JsonModelCollection JsonModels;
    public Transform ModelsContainer;
    public UnityEvent OnModelsLoaded = new UnityEvent();
    public ModelEvent OnModelCreated = new ModelEvent();

    public List<Model> Models { get; set; }
    public Model CurrentModel { get; set; }

    public readonly string JSON_URL = "https://s3-sa-east-1.amazonaws.com/static-files-prod/unity3d/models.json";

    private int _numberOfModels;
    private int _numberOfDuplications;

    void Start()
    {

    }

    void Update()
    {
        
    }

    public void Initialize(JsonModelCollection jsonClass, bool loadedFromSave)
    {
        Models = new List<Model>();
        JsonModels = jsonClass;

        LoadJsonModels(loadedFromSave);

        LoadResources();
        LoadModelResources();
        InstantiateObjects();

        if (OnModelsLoaded != null)
            OnModelsLoaded.Invoke();
    }

    private void LoadJsonModels(bool loadedFromSave)
    {
        _numberOfModels = 0;

        foreach (JsonModel jsonModel in JsonModels.models)
        {
            Model model = jsonModel;
            model.ObjectIndex = _numberOfModels;

            if(!loadedFromSave)
                model.Index = _numberOfModels;

            _numberOfModels++;
            Models.Add(model);
        }
    }

    private void LoadResources()
    {
        ResourceModels = new List<ResourceModel>();

        if (ResourceFolders.Count() == 0)
        {
            Debug.LogError("Resource Folders are empty on ModelsManager");
            return;
        }

        foreach (ResourceFolder path in ResourceFolders)
        {
            GameObject modelObject = Resources.Load<GameObject>(path.Folder + path.Prefab);
            Sprite modelIcon = Resources.Load<Sprite>(path.Folder + path.Icon);

            List<(Texture, Sprite)> modelTextures = new List<(Texture, Sprite)>();
            for (int i = 0; i < path.Textures.Length; i++)
                modelTextures.Add((Resources.Load<Texture>(path.Folder + path.Textures[i]), Resources.Load<Sprite>(path.Folder + path.TextureIcons[i])));

            ResourceModel model = new ResourceModel();
            model.Object = modelObject;
            model.Icon = modelIcon;
            model.Textures = modelTextures.Select(tuple => tuple.Item1).ToArray();
            model.TextureIcons = modelTextures.Select(tuple => tuple.Item2).ToArray();
            model.Colors = path.Colors;

            ResourceModels.Add(model);
        }
    }

    private void LoadModelResources()
    {
        foreach (Model model in Models)
        {
            int modelIndex = model.Index;

            if(modelIndex >= ResourceModels.Count)
            {
                Debug.LogError("Model index " + modelIndex + " > number of Resources.");
                modelIndex = ResourceModels.Count - 1;
                model.Index = modelIndex;
            }

            ResourceModel resource = ResourceModels[model.Index];

            model.Object = resource.Object;
            model.Icon = resource.Icon;
            model.Textures = resource.Textures;
            model.TextureIcons = resource.TextureIcons;
            model.Colors = resource.Colors;
        }
    }

    private void InstantiateObjects()
    {
        for (int i = 0; i < Models.Count; i++)
        {
            print("Instantiating " + Models[i].Name + ": Prefab Index " + Models[i].Index);

            Model model = Models[i];
            GameObject modelObject = Instantiate(model.Object, ModelsContainer);

            Model newModel = modelObject.GetComponent<Model>();
            newModel.Object = modelObject;
            newModel.SetModelBase(model);

            if (OnModelCreated != null)
                OnModelCreated.Invoke(newModel);

            Models[i] = newModel;
        }
    }

    public void SaveModels()
    {
        JsonModels.models = new List<JsonModel>();

        foreach (Model model in Models)
        {
            model.UpdateTransform();
            JsonModels.models.Add(model);
        }
    }

    public void EraseModels()
    {
        foreach (Model model in Models)
        {
            Destroy(model.Object);
        }

        Models.Clear();
        _numberOfModels = 0;
    }

    public void SetCurrentModel(Model model)
    {
        CurrentModel = model;
        _numberOfDuplications = 0;
    }

    public Model DuplicateModel(Model model)
    {
        _numberOfDuplications++;

        Vector3 position = model.transform.position + (Vector3.forward * _numberOfDuplications);
        GameObject modelObject = Instantiate(model.Object, position, model.transform.rotation, ModelsContainer);

        Model newModel = modelObject.GetComponent<Model>();
        newModel.Object = modelObject;
        newModel.SetModelBase(model);

        _numberOfModels++;
        newModel.ObjectIndex = _numberOfModels;

        if (OnModelCreated != null)
            OnModelCreated.Invoke(newModel);

        Models.Add(newModel);
        return newModel;
    }

    public void DeleteModel(int index)
    {
        Model model = Models.Find(x => x.Index == index);
        Destroy(model.Object);

        Models.Remove(model);
        _numberOfModels--;
    }

}
