﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelRenderer : MonoBehaviour
{
    public Renderer Renderer;

    private Material _material;
    private Texture _baseTexture;
    private Color _baseColor;
    private Color _emissiveColor;

    private bool _initialized;

    void Start()
    {
        if(!_initialized)
            Initialize();
    }

    void Update()
    {
        
    }

    public void Initialize()
    {
        _initialized = true;

        if (Renderer == null)
            Renderer = GetComponent<Renderer>();

        if (Renderer == null)
            Renderer = GetComponentInChildren<Renderer>();

        Renderer.material = new Material(Renderer.material);

        _material = Renderer.material;
        _baseTexture = _material.GetTexture("_BaseColorMap");
        _baseColor = _material.GetColor("_BaseColor");
        _emissiveColor = _material.GetColor("_EmissiveColor");
    }

    public void SetBaseTexture(Texture texture)
    {
        _material.SetTexture("_BaseColorMap", texture);
    }

    public void SetBaseColor(Color color)
    {
        _material.SetColor("_BaseColor", color);
    }

    public void SetEmissionColor(Color color)
    {
        _material.SetColor("_EmissiveColor", color);
    }

    public void SetEmissionColor(Color color, float intensity)
    {
        _material.SetColor("_EmissiveColor", color * intensity);
    }

    public void SetEmissionIntensity(float intensity)
    {
        Color color = _emissiveColor * intensity;

        _material.SetColor("_EmissiveColor", color);
    }
}
