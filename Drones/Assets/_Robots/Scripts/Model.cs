﻿using System;
using UnityEngine;

[Serializable]
public class Model : MonoBehaviour
{
    [Header("Configuration")]
    public string Name;
    public Vector3 Position;
    public Vector3 Rotation;
    public Vector3 Scale;
    public int Index;
    public int ColorOption;
    public int TextureOption;

    [Header("Resources")]
    public GameObject Object;
    public Sprite Icon;
    public Texture[] Textures;
    public Sprite[] TextureIcons;
    public Color[] Colors;

    [Header("References")]
    public ModelRenderer Renderer;
    public SelectableObject Selectable;

    public int ObjectIndex { get; set; }

    public static implicit operator JsonModel(Model model)
    {
        JsonModel jsonModel = new JsonModel();
        jsonModel.name = model.Name;
        jsonModel.position = jsonModel.Convert(model.Position);
        jsonModel.rotation = jsonModel.Convert(model.Rotation);
        jsonModel.scale = jsonModel.Convert(model.Scale);
        jsonModel.index = model.Index;
        jsonModel.colorOption = model.ColorOption;
        jsonModel.textureOption = model.TextureOption;

        return jsonModel;
    }

    void Start()
    {

    }

    void Update()
    {

    }

    public void SetModelBase(Model other)
    {
        Object.name = Object.name.Replace("(Clone)", "") + ": " + other.Name;
        Object.transform.position = other.Position;
        Object.transform.eulerAngles = other.Rotation;
        Object.transform.localScale = other.Scale;

        Name = other.Name;
        Position = other.Position;
        Rotation = other.Rotation;
        Scale = other.Scale;
        Index = other.Index;
        ColorOption = other.ColorOption;
        TextureOption = other.TextureOption;

        Icon = other.Icon;
        Textures = other.Textures;
        TextureIcons = other.TextureIcons;
        Colors = other.Colors;

        Renderer = Object.GetComponent<ModelRenderer>();
        Selectable = Object.GetComponent<SelectableObject>();
        Selectable.Model = this;

        Renderer.Initialize();
        ChangeColor(ColorOption);
        ChangeTexture(TextureOption);
    }

    public void UpdateTransform()
    {
        Position = Object.transform.position;
        Rotation = Object.transform.eulerAngles;
        Scale = Object.transform.localScale;
    }
 
    public void ChangeTexture(int options)
    {
        TextureOption = options;
        Renderer.SetBaseTexture(Textures[options]);
    }

    public void ChangeColor(int options)
    {
        ColorOption = options;
        Renderer.SetBaseColor(Colors[options]);
    }

}
