﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SelectableObject : MonoBehaviour
{
    [Header("References")]
    public Collider Collider;
    public Transform CameraTarget;

    [Header("Events")]
    public UnityEvent OnSelected = new UnityEvent();
    public UnityEvent OnDeselected = new UnityEvent();

    public bool Selected { get; set; }
    public int Index { get; set; }
    public Model Model { get; set; }


    void Start()
    {
        ResetSelection();
    }

    void Update()
    {
        
    }

    void OnMouseDown()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        Select();
    }

    public virtual void Select()
    {
        if (Selected)
            return;

        Selected = true;
        Collider.enabled = false;

        if (OnSelected != null)
            OnSelected.Invoke();
    }

    public virtual void Deselect()
    {
        if (!Selected)
            return;

        Selected = false;
        Collider.enabled = true;

        if (OnDeselected != null)
            OnDeselected.Invoke();
    }

    public void ResetSelection()
    {
        Collider.enabled = true;
        Selected = false;
    }
}
