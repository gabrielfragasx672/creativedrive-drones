﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class SaveClass
{
    public object Class;
    public string FileName;

    public SaveClass(object classObject, string fileName)
    {
        Class = classObject;
        FileName = fileName;
    }

    public SaveClass(string fileName)
    {
        FileName = fileName;
    }
}

public class SaveManager : MonoBehaviour
{
    public List<SaveClass> Classes = new List<SaveClass>();
    public bool EraseSaveOnPlay;

    private string _SAVEPATH { get { return Application.persistentDataPath; } }

    void Start()
    {
        if (EraseSaveOnPlay)
            EraseSaveSystem();
    }

    void Update()
    {
        
    }

    public void CreateSaveSystem()
    {
        PlayerPrefs.SetInt("SaveSystem", 0);
        PlayerPrefs.Save();
    }

    public bool HasSaveSystem()
    {
        return PlayerPrefs.HasKey("SaveSystem");
    }

    public void EraseSaveSystem()
    {
        PlayerPrefs.DeleteKey("SaveSystem");
        PlayerPrefs.Save();
    }

    public void SetSaveClass(object classObject, string fileName)
    {
        SaveClass newClass = new SaveClass(classObject, fileName);
        bool isNewClass = true;

        foreach (SaveClass @class in Classes)
        {
            if (@class.FileName == fileName || @class.Class.GetType() == classObject.GetType())
            {
                @class.Class = classObject;
                @class.FileName = fileName;
                isNewClass = false;
                break;
            }
        }

        if(isNewClass)
            Classes.Add(newClass);
    }

    public void SetClassToLoad(string fileName)
    {
        SaveClass @class = new SaveClass(null, fileName);
        Classes.Add(@class);
    }

    public object GetClassFrom(string fileName)
    {
        foreach (SaveClass @class in Classes)
        {
            if (@class.FileName == fileName)
            {
                return @class.Class;
            }
        }

        return null;
    }   
    
    public bool ClassExists(string fileName)
    {
        string filePath = _SAVEPATH + "/" + fileName;
        return File.Exists(filePath);
    }

    public void SaveClasses()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        
        foreach (SaveClass @class in Classes)
        {
            FileStream file = File.Create(_SAVEPATH + "/" + @class.FileName);
            formatter.Serialize(file, @class.Class);
            file.Close();

            Debug.Log("Saved: " + @class.Class.GetType().FullName + " at " +  @class.FileName);
        }

        Debug.Log("Game Saved");
    }

    public void LoadClasses()
    {
        foreach (SaveClass @class in Classes)
        {
            string filePath = _SAVEPATH + "/" + @class.FileName;

            if (File.Exists(filePath))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream file = File.Open(filePath, FileMode.Open);
                @class.Class = formatter.Deserialize(file);
                file.Close();

                Debug.Log("Saved: " + @class.Class.GetType().FullName + " at " + @class.FileName);
            }
            else
            {
                Debug.Log("File not found: " + @class.FileName);
                Debug.Log("Path: " + filePath);
            }
        }

        Debug.Log("Game Loaded");
    }

    public object LoadClass(string fileName)
    {
        string path = _SAVEPATH + "/" + fileName;

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream file = File.Open(path, FileMode.Open);
            object @class = formatter.Deserialize(file);
            file.Close();

            Debug.Log("Saved: " + @class.GetType().FullName + " at " + path);
            return @class;
        }
        else
        {
            Debug.Log("File not found: " + fileName);
            Debug.Log("Path: " + path);
        }

        return null;
    }
}
