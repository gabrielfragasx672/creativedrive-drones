﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
    #region Classes
    [Serializable] public class CanvasEvents
    {
        public class ToggleEvent : UnityEvent<int> { }

        public UnityEvent OnPositionToggled;
        public UnityEvent OnRotationToggled;
        public UnityEvent OnScaleToggled;
        public UnityEvent OnDuplicateButton;
        public UnityEvent OnDeleteButton;
        public UnityEvent OnSaveButton;
        public UnityEvent OnLoadButton;
        public UnityEvent OnEraseButton;
        public UnityEvent OnQuitButton;
        public ToggleEvent OnColorSelected;
        public ToggleEvent OnTextureSelected;
        public ToggleEvent OnModelSelected;

        public CanvasEvents()
        {
            OnPositionToggled = new UnityEvent();
            OnRotationToggled = new UnityEvent();
            OnScaleToggled = new UnityEvent();
            OnDuplicateButton = new UnityEvent();
            OnDeleteButton = new UnityEvent();
            OnSaveButton = new UnityEvent();
            OnLoadButton = new UnityEvent();
            OnEraseButton = new UnityEvent();
            OnQuitButton = new UnityEvent();
            OnColorSelected = new ToggleEvent();
            OnTextureSelected = new ToggleEvent();
            OnModelSelected = new ToggleEvent();
        }
    }

    [Serializable] public class CanvasToggle
    {
        public GameObject Object;
        public Toggle Toggle;
        public Image Image;

        public int Index { get; set; }
    }

    [Serializable] public class CanvasPanel
    {
        public GameObject Object;
        public Tween Tween;

        public bool IsActive { get; set; }

        public void Open()
        {
            if (IsActive)
                return;

            SetActive(true);

            if (Tween.IsPlaying)
                Tween.Stop();

            Tween.Play();
        }

        public void Close()
        {
            if (!IsActive)
                return;

            if (Tween.OnReverseCompleted.GetPersistentEventCount() == 0)
                Tween.OnReverseCompleted.AddListener((t) => SetActive(false));

            if (Tween.IsPlaying)
                Tween.Stop();

            Tween.Reverse();
        }

        public void SetActive(bool active)
        {
            IsActive = active;
            Object.SetActive(active);
        }
    }
    #endregion

    [Header("Panels")]
    public CanvasPanel ModelPanel;
    public CanvasPanel TransformPanel;
    public CanvasPanel EditPanel;
    public CanvasPanel SavePanel;

    [Header("Buttons")]
    public CanvasToggle[] ColorButtons;
    public CanvasToggle[] TextureButtons;
    public List<CanvasToggle> ModelButtons;

    [Header("Events")]
    public CanvasEvents Events;

    public int ColorOptionSelected { get; set; }
    public int TextureOptionSelected { get; set; }

    private bool _usedFirstModelButton;

    void Start()
    {

    }

    void Update()
    {
        
    }

    public void InitializeEvents()
    {
        Events = new CanvasEvents();
    }

    #region Toggle Functions
    public void PositionToggle(bool toggleOn)
    {
        if (toggleOn)
        {
            EditPanel.Close();

            if (Events.OnPositionToggled != null)
                Events.OnPositionToggled.Invoke();
        }
    }

    public void RotationToggle(bool toggleOn)
    {
        if (toggleOn)
        {
            EditPanel.Close();

            if (Events.OnRotationToggled != null)
                Events.OnRotationToggled.Invoke();
        }
    }

    public void ScaleToggle(bool toggleOn)
    {
        if (toggleOn)
        {
            EditPanel.Close();

            if (Events.OnScaleToggled != null)
                Events.OnScaleToggled.Invoke();
        }
    }

    public void ColorToggle(bool toggleOn)
    {
        if (toggleOn)
        {
            EditPanel.Open();

            SetTextureButtonsActive(false);
            SetColorButtonsActive(true);
        }
    }

    public void ColorSelectionToggle(bool toggleOn)
    {
        if (toggleOn)
        {
            if (Events.OnColorSelected != null)
                Events.OnColorSelected.Invoke(ColorOptionSelected);
        }
    }

    public void TextureToggle(bool toggleOn)
    {
        if (toggleOn)
        {
            EditPanel.Open();

            SetColorButtonsActive(false);
            SetTextureButtonsActive(true);
        }
    }

    public void TextureSelectionToggle(bool toggleOn)
    {
        if (toggleOn)
        {
            if (Events.OnTextureSelected != null)
                Events.OnTextureSelected.Invoke(TextureOptionSelected);
        }
    }
    #endregion

    #region Button Functions
    public void ButtonDuplicateClick()
    {
        if (Events.OnDuplicateButton != null)
            Events.OnDuplicateButton.Invoke();
    }

    public void ButtonDeleteClick()
    {
        if (Events.OnDeleteButton != null)
            Events.OnDeleteButton.Invoke();
    }

    public void ButtonSaveClick()
    {
        if (Events.OnSaveButton != null)
            Events.OnSaveButton.Invoke();
    }

    public void ButtonLoadClick()
    {
        if (Events.OnLoadButton != null)
            Events.OnLoadButton.Invoke();
    }

    public void ButtonEraseClick()
    {
        if (Events.OnEraseButton != null)
            Events.OnEraseButton.Invoke();
    }

    public void ButtonQuitClick()
    {
        if (Events.OnQuitButton != null)
            Events.OnQuitButton.Invoke();
    }

    public void ButtonModelPanelClick(Transform buttonTransform)
    {
        if(ModelPanel.IsActive)
        {
            buttonTransform.eulerAngles = Vector3.zero;
            ModelPanel.Close();
        }
        else
        {
            buttonTransform.eulerAngles = Vector3.forward * 180;
            ModelPanel.Open();
        }
    }

    public void ButtonSavePanelClick(Transform buttonTransform)
    {
        if (SavePanel.IsActive)
        {
            buttonTransform.eulerAngles = Vector3.zero;
            SavePanel.Close();
        }
        else
        {
            buttonTransform.eulerAngles = Vector3.forward * 180;
            SavePanel.Open();
        }
    }
    #endregion

    #region Customization Buttons Functions
    public void AddModelButton(Sprite sprite, int index)
    {
        CanvasToggle firstToggle = ModelButtons[0];

        if (ModelButtons.Count == 1 && !_usedFirstModelButton)
        {
            _usedFirstModelButton = true;

            firstToggle.Index = index;
            firstToggle.Image.sprite = sprite;
            firstToggle.Toggle.onValueChanged.AddListener((isOn) =>
            {
                if(isOn)
                {
                    if (Events.OnModelSelected != null)
                        Events.OnModelSelected.Invoke(firstToggle.Index);
                }
            });
        }
        else
        {
            CanvasToggle newToggle = new CanvasToggle();
            newToggle.Object = Instantiate(firstToggle.Object, firstToggle.Object.transform.parent);
            newToggle.Image = newToggle.Object.transform.Find("Thumbnail").GetComponent<Image>();
            newToggle.Toggle = newToggle.Object.GetComponent<Toggle>();

            newToggle.Index = index;
            newToggle.Image.sprite = sprite;
            newToggle.Toggle.isOn = false;
            newToggle.Toggle.onValueChanged.AddListener((isOn) =>
            {
                if (isOn)
                {
                    if (Events.OnModelSelected != null)
                        Events.OnModelSelected.Invoke(newToggle.Index);
                }
            });

            ModelButtons.Add(newToggle);
        }
    }

    public void RemoveModelButton(int index)
    {
        if(ModelButtons.Count == 1)
        {
            ModelButtons[0].Object.SetActive(false);
            _usedFirstModelButton = false;
        }
        else
        {
            for (int i = 0; i < ModelButtons.Count; i++)
            {
                if (ModelButtons[i].Index == index)
                {
                    Destroy(ModelButtons[i].Object);
                    ModelButtons.Remove(ModelButtons[i]);
                }
            }
        }
    }

    public void UpdateColorButtons(Color[] colors, int optionSelected)
    {
        for (int i = 0; i < colors.Length; i++)
        {
            ColorButtons[i].Image.color = colors[i];
        }

        ColorOptionSelected = optionSelected;
        ColorButtons[optionSelected].Toggle.isOn = true;
    }

    public void UpdateTextureButtons(Sprite[] sprites, int optionSelected)
    {
        for (int i = 0; i < sprites.Length; i++)
        {
            TextureButtons[i].Image.sprite = sprites[i];
        }

        TextureOptionSelected = optionSelected;
        TextureButtons[optionSelected].Toggle.isOn = true;
    }

    private void SetColorButtonsActive(bool active)
    {
        for (int i = 0; i < ColorButtons.Length; i++)
        {
            ColorButtons[i].Object.SetActive(active);
        }
    }

    private void SetTextureButtonsActive(bool active)
    {
        for (int i = 0; i < TextureButtons.Length; i++)
        {
            TextureButtons[i].Object.SetActive(active);
        }
    }
    #endregion
}
