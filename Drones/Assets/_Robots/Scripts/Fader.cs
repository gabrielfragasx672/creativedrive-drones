﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    public Tween FaderTween;
    public Image FaderImage;
    public Text Loading;
    public float Delay;

    private Color _color;

    void Start()
    {
        if(Loading != null)
            StartCoroutine(LoadingRoutine());

        Invoke("StartFade", Delay);
    }

    void Update()
    {
        if (FaderTween.IsPlaying)
            FaderImage.color = new Color(_color.r, _color.g, _color.b, FaderTween.FloatValue);
    }

    public void LoadGameScene()
    {
        if (FaderTween.IsPlaying)
            return;

        FaderTween.gameObject.SetActive(true);

        FaderTween.OnCompleted.RemoveAllListeners();
        FaderTween.OnCompleted.AddListener((t) =>
        {
            SceneManager.LoadScene("Game");
        });

        FaderTween.SetDuration(1).SetFloat(0, 1).Play();
    }

    private void StartFade()
    {
        if (Loading != null)
        {
            Loading.gameObject.SetActive(false);
            StopCoroutine(LoadingRoutine());
        }

        FaderTween.OnCompleted.AddListener((t) =>
        {
            FaderTween.gameObject.SetActive(false);
        });

        _color = FaderImage.color;
        FaderTween.SetDuration(1).SetFloat(1, 0).Play();
    }

    private IEnumerator LoadingRoutine()
    {
        Loading.text = Loading.text.Replace("...", "");

        yield return new WaitForSeconds(0.2f);

        Loading.text += ".";

        yield return new WaitForSeconds(0.2f);

        Loading.text += ".";

        yield return new WaitForSeconds(0.2f);

        Loading.text += ".";

        yield return new WaitForSeconds(0.2f);

        StartCoroutine(LoadingRoutine());
    }
}
