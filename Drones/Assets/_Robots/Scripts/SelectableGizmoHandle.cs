using UnityEngine;
using System.Collections;

public class SelectableGizmoHandle : MonoBehaviour 
{
    public SelectableGizmo Gizmo;
    public GizmoControl Control;
    public GizmoTypes Type;
    public GameObject PositionCap;
    public GameObject RotationCap;
    public GameObject ScaleCap;
    public Material ActiveMaterial;
    public GizmoAxis Axis;

    public float MoveSensitivity = 10f;
    public float RotationSensitivity = 64f;
    public float ScaleSensitivity = 10f;

    private Material _inactiveMaterial;
    private bool _selected;

    void Awake()
    {
        _inactiveMaterial = GetComponent<Renderer>().material;
    }

    void OnMouseDown()
    {
        Gizmo.DeselectHandles();
        Selected();
    }

    void Update()
    {

    }

    void OnMouseDrag()
    {
        var delta = 0f;
        var vertical = 0f;
        var horizontal = 0f;

        if (_selected)
        {
            horizontal = Input.GetAxis("Mouse X") * Time.deltaTime;
            vertical = Input.GetAxis("Mouse Y") * Time.deltaTime;

            switch (Control)
            {
                case GizmoControl.Horizontal:
                    delta = Input.GetAxis("Mouse X") * Time.deltaTime;
                    break;
                case GizmoControl.Vertical:
                    delta = Input.GetAxis("Mouse Y") * Time.deltaTime;
                    break;
                case GizmoControl.Both:
                    delta = (Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y")) * Time.deltaTime;
                    break;
            }

            switch (Type)
            {
                case GizmoTypes.Position:
                    delta *= MoveSensitivity;
                    horizontal *= MoveSensitivity;
                    vertical *= MoveSensitivity;
                    switch (Axis)
                    {
                        case GizmoAxis.X:
                            Gizmo.TargetObject.Translate(Vector3.right * delta, Space.Self);
                            break;
                        case GizmoAxis.Y:
                            Gizmo.TargetObject.Translate(Vector3.up * delta, Space.Self);
                            break;
                        case GizmoAxis.Z:
                            Gizmo.TargetObject.Translate(Vector3.forward * delta, Space.Self);
                            break;
                        case GizmoAxis.Center:
                            Gizmo.TargetObject.Translate(Vector3.right * horizontal, Space.Self);
                            Gizmo.TargetObject.Translate(Vector3.forward * vertical, Space.Self);
                            break;
                    }
                    break;

                case GizmoTypes.Scale:
                    var obj = Gizmo.TargetObject;
                    delta *= ScaleSensitivity;
                    switch (Axis)
                    {
                        case GizmoAxis.X:
                            Gizmo.TargetObject.localScale = new Vector3(obj.localScale.x + delta, obj.localScale.y, obj.localScale.z);
                            break;
                        case GizmoAxis.Y:
                            Gizmo.TargetObject.localScale = new Vector3(obj.localScale.x, obj.localScale.y + delta, obj.localScale.z);
                            break;
                        case GizmoAxis.Z:
                            Gizmo.TargetObject.localScale = new Vector3(obj.localScale.x, obj.localScale.y, obj.localScale.z + delta);
                            break;
                        case GizmoAxis.Center:
                            Gizmo.TargetObject.localScale = new Vector3(obj.localScale.x + delta, obj.localScale.y + delta, obj.localScale.z + delta);
                            break;
                    }
                    break;

                case GizmoTypes.Rotation:
                    delta *= RotationSensitivity;
                    switch (Axis)
                    {
                        case GizmoAxis.X:
                            Gizmo.TargetObject.Rotate(Vector3.right * delta);
                            break;
                        case GizmoAxis.Y:
                            Gizmo.TargetObject.Rotate(Vector3.up * delta);
                            break;
                        case GizmoAxis.Z:
                            Gizmo.TargetObject.Rotate(Vector3.forward * delta);
                            break;
                        case GizmoAxis.Center:
                            Gizmo.TargetObject.Rotate(Vector3.right * delta);
                            Gizmo.TargetObject.Rotate(Vector3.up * delta);
                            Gizmo.TargetObject.Rotate(Vector3.forward * delta);
                            break;
                    }
                    break;
            }
        }
    }

    public void Selected()
    {
        _selected = true;
        GetComponent<Renderer>().material = ActiveMaterial;

        if (Axis != GizmoAxis.Center)
        {
            PositionCap.GetComponent<Renderer>().material = ActiveMaterial;
            RotationCap.GetComponent<Renderer>().material = ActiveMaterial;
            ScaleCap.GetComponent<Renderer>().material = ActiveMaterial;
        }
    }

    public void Deselected()
    {
        _selected = false;
        GetComponent<Renderer>().material = _inactiveMaterial;
        if (Axis != GizmoAxis.Center)
        {
            PositionCap.GetComponent<Renderer>().material = _inactiveMaterial;
            RotationCap.GetComponent<Renderer>().material = _inactiveMaterial;
            ScaleCap.GetComponent<Renderer>().material = _inactiveMaterial;
        }
    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    public void SetType(GizmoTypes type)
    {
        Type = type;
        if (Axis != GizmoAxis.Center)
        {
            PositionCap.SetActive(type == GizmoTypes.Position);
            RotationCap.SetActive(type == GizmoTypes.Rotation);
            ScaleCap.SetActive(type == GizmoTypes.Scale);
        }
    }

}
