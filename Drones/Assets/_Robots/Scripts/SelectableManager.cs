﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class SelectableManager : MonoBehaviour
{
    public class SelectableEvent : UnityEvent<SelectableObject> { }

    public SelectableGizmo Gizmo;
    public SelectableCamera Camera;

    public UnityEvent OnSelectablesLoaded = new UnityEvent();
    public SelectableEvent OnSelection = new SelectableEvent();

    [HideInInspector]
    public UnityEvent OnClearSelection = new UnityEvent();

    public List<SelectableObject> Selectables { get; set; }
    public SelectableObject CurrentSelected { get; set; }

    private int _numberOfObjects;
    private int _numberOfDuplications;

    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (CurrentSelected != null)
                Camera.FocusOn(CurrentSelected);
        }

        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
                return;
        }
    }

    public void Initialize()
    {
        InitializeSelectables();

        if (OnSelectablesLoaded != null)
            OnSelectablesLoaded.Invoke();
    }

    private void InitializeSelectables()
    {
        if (Selectables == null)
        {
            Selectables = FindObjectsOfType<SelectableObject>().ToList();
        }

        foreach (SelectableObject selectable in Selectables)
        {
            InitializeSelectableObject(selectable);
        }
    }

    private void InitializeSelectableObject(SelectableObject selectable)
    {
        selectable.Index = _numberOfObjects;

        selectable.OnSelected.AddListener(() =>
        {
            RegisterSelection(selectable);
        });

        _numberOfObjects++;
    }

    private void RegisterSelection(SelectableObject selected)
    {
        if (CurrentSelected != null)
            CurrentSelected.Deselect();
        
        CurrentSelected = selected;

        Gizmo.SetTarget(selected.transform);
        Gizmo.Activate();

        if (OnSelection != null)
            OnSelection.Invoke(CurrentSelected);
    }

    private void ClearSelection()
    {
        if (CurrentSelected != null)
        {
            CurrentSelected.Deselect();
            Gizmo.Deactivate();

            if (OnClearSelection != null)
                OnClearSelection.Invoke();
        }
    }

    public void SetPositionGizmo()
    {
        Gizmo.SetType(GizmoTypes.Position);
    }

    public void SetRotationGizmo()
    {
        Gizmo.SetType(GizmoTypes.Rotation);
    }

    public void SetScaleGizmo()
    {
        Gizmo.SetType(GizmoTypes.Scale);
    }

    public void AddSelectable(SelectableObject selectable)
    {
        InitializeSelectableObject(selectable);
        selectable.ResetSelection();

        Selectables.Add(selectable);
    }

    public void RemoveSelectable(SelectableObject selectable)
    {
        ClearSelection();

        Selectables.Remove(selectable);
        _numberOfObjects--;
    }

    public void FindSelectable(int index)
    {
        SelectableObject selected = Selectables.Find(o => o.Index == index);
        selected.Select();

        Camera.FocusOn(selected);
    }

    public void Clear()
    {
        _numberOfObjects = 0;

        ClearSelection();
        Selectables.Clear();
    }
}
