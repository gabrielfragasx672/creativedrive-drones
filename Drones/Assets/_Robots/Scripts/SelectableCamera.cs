﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableCamera : MonoBehaviour
{
    public Camera Camera;
    public CameraMovement Movement;

    private Transform _target;
    private Tween _tween;
    private bool _focusing;

    void Start()
    {
        _tween = Tween.AttachTo(gameObject);

        _tween.OnStarted.AddListener((t) =>
        {
            Movement.enabled = false;
            transform.LookAt(_target);

            _focusing = true;
        });

        _tween.OnCompleted.AddListener((t) =>
        {
            transform.LookAt(_target);
            _focusing = false;

            Movement.enabled = true;
            Movement.boost = 1.5f;
        });
    }

    void Update()
    {
        if(_focusing)
            transform.LookAt(_target);
    }

    public void FocusOn(SelectableObject selectable)
    {
        if(_focusing || _tween.IsPlaying)
        {
            _tween.Stop();
        }

        _target = selectable.CameraTarget;
        _tween.SetDuration(0.25f).SetPosition(transform.position, _target.position + (Vector3.one * 1.2f)).Play();
    }
}
