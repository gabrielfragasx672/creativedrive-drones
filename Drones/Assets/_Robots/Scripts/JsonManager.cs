﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class JsonManager : MonoBehaviour
{
    public UnityEvent OnJsonLoaded = new UnityEvent();
    public UnityEvent OnJsonError = new UnityEvent();
    public string JsonText { get; set; }

    void Start()
    {

    }

    void Update()
    {

    }

    public void LoadJsonFromUrl(string url)
    {
        StartCoroutine(ReadJsonFromUrl(url));
    }

    public object GetJsonClass<T>()
    {
        return JsonUtility.FromJson<T>(JsonText);
    }

    public string SetJsonClass(object objectClass, bool formatString = true)
    {
        return JsonUtility.ToJson(objectClass, formatString);
    }

    private IEnumerator ReadJsonFromUrl(string url)
    {
        UnityWebRequest www = UnityWebRequest.Get(url);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            if (OnJsonError != null)
                OnJsonError.Invoke();

            Debug.Log("JSON ERROR: " + www.error);
        }
        else
        {
            byte[] bytes = www.downloadHandler.data;
            string jsonText = System.Text.Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);

            JsonText = jsonText;

            if (OnJsonLoaded != null)
                OnJsonLoaded.Invoke();

            Debug.Log("JSON LOADED: " + jsonText);
        }
    }
}
