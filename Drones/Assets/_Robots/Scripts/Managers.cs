﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Managers : MonoBehaviour
{
    public SelectableManager Selectables;
    public ModelsManager Models;
    public CanvasManager Canvas;
    public SaveManager Save;
    public JsonManager Json;

    void Start()
    {
        if (Save.HasSaveSystem())
        {
            print("Load Save System");

            InitializeModelsFromSave();
        }
        else
        {
            print("Create Save System");

            Json.LoadJsonFromUrl(Models.JSON_URL);

            Json.OnJsonLoaded.AddListener(() =>
            {
                InitializeModelsFromJson();
            });
        }
    }

    
    void Update()
    {
        
    }

    private void InitializeModelsFromJson()
    {
        Models.OnModelCreated.AddListener((model) =>
        {
            Canvas.AddModelButton(model.Icon, model.ObjectIndex);
        });

        Models.OnModelsLoaded.AddListener(() =>
        {
            Save.CreateSaveSystem();
            Save.SetSaveClass(Models.JsonModels, "models.json");
            Save.SaveClasses();

            InitializeCanvas();
            InitializeSelectables();
        });

        Models.Initialize((JsonModelCollection)Json.GetJsonClass<JsonModelCollection>(), false);
    }

    private void InitializeModelsFromSave()
    {
        Save.SetClassToLoad("models.json");
        Save.LoadClasses();

        Models.OnModelCreated.RemoveAllListeners();
        Models.OnModelsLoaded.RemoveAllListeners();

        Models.OnModelCreated.AddListener((model) =>
        {
            Canvas.AddModelButton(model.Icon, model.ObjectIndex);
        });

        Models.OnModelsLoaded.AddListener(() =>
        {
            print("loaded models");
            InitializeCanvas();
            InitializeSelectables();
        });

        Models.Initialize((JsonModelCollection)Save.GetClassFrom("models.json"), true);
    }

    private void InitializeSelectables()
    {
        Selectables.OnSelectablesLoaded.AddListener(() =>
        {

        });

        Selectables.OnSelection.AddListener((current) =>
        {
            Model model = current.Model;
            Models.SetCurrentModel(model);

            Canvas.UpdateColorButtons(model.Colors, model.ColorOption);
            Canvas.TransformPanel.Open();
        });

        Selectables.OnClearSelection.AddListener(() =>
        {
            Canvas.TransformPanel.Close();
            Canvas.EditPanel.Close();
        });

        Selectables.Initialize();
    }

    private void InitializeCanvas()
    {
        Canvas.InitializeEvents();

        RegisterTransformEvents();
        RegisterModelEvents();
        RegisterSaveEvents();
    }

    private void RegisterTransformEvents()
    {
        Canvas.Events.OnPositionToggled.AddListener(() =>
        {
            Selectables.SetPositionGizmo();
        });

        Canvas.Events.OnRotationToggled.AddListener(() =>
        {
            Selectables.SetRotationGizmo();
        });

        Canvas.Events.OnScaleToggled.AddListener(() =>
        {
            Selectables.SetScaleGizmo();
        });
    }

    private void RegisterModelEvents()
    {
        Canvas.Events.OnColorSelected.AddListener((option) =>
        {
            Models.CurrentModel.ChangeColor(option);
        });

        Canvas.Events.OnTextureSelected.AddListener((option) =>
        {
            Models.CurrentModel.ChangeTexture(option);
        });

        Canvas.Events.OnModelSelected.AddListener((index) =>
        {
            Selectables.FindSelectable(index);
        });

        Canvas.Events.OnDuplicateButton.AddListener(() =>
        {
            Model current = Models.CurrentModel;
            Model newModel = Models.DuplicateModel(current);

            Selectables.AddSelectable(newModel.Selectable);
        });

        Canvas.Events.OnDeleteButton.AddListener(() =>
        {
            Model current = Models.CurrentModel;

            Selectables.RemoveSelectable(current.Selectable);
            Models.DeleteModel(current.Index);
        });
    }

    private void RegisterSaveEvents()
    {
        Canvas.Events.OnSaveButton.AddListener(() =>
        {
            Models.SaveModels();

            Save.SetSaveClass(Models.JsonModels, "models.json");
            Save.SaveClasses();
        });

        Canvas.Events.OnLoadButton.AddListener(() =>
        {
            Selectables.Clear();
            Models.EraseModels();

            InitializeModelsFromSave();
        });

        Canvas.Events.OnEraseButton.AddListener(() =>
        {
            Save.EraseSaveSystem();

            Selectables.Clear();
            Models.EraseModels();
        });

        Canvas.Events.OnQuitButton.AddListener(() =>
        {
            Selectables.Clear();
            Models.EraseModels();
            Application.Quit();
        });
    }
}
