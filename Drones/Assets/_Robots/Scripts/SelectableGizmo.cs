using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#region Enums
public enum GizmoTypes { Position, Rotation, Scale }
public enum GizmoControl { Horizontal, Vertical, Both }
public enum GizmoAxis { Center, X, Y, Z }
#endregion

public class SelectableGizmo : MonoBehaviour 
{
    public SelectableGizmoHandle AxisCenter;
    public SelectableGizmoHandle AxisX;
    public SelectableGizmoHandle AxisY;
    public SelectableGizmoHandle AxisZ;
    public GizmoTypes Type;
    public Transform TargetObject;
    public float DefaultDistance = 3.2f;
    public float ScaleFactor = 0.2f;
    
    private Vector3 _localScale;
    private GizmoTypes _type;
    private Camera _camera;
    private Transform _parent;
    private bool _visible;


    void Awake()
    {
        _localScale = transform.localScale;
        _parent = transform.parent;
        _camera = Camera.main;

        InitializeHandles();
        SetType(GizmoTypes.Position);
        Deactivate();
    }
	
	void Update () 
    {
        if (_visible)
        {
            if(_type != Type)
                SetType(Type);

            if (TargetObject != null)
            {
                transform.position = TargetObject.position;
                transform.rotation = TargetObject.rotation;
            }

            float distance = Vector3.Distance(transform.position, _camera.transform.position);
            float scale = (distance - DefaultDistance) * ScaleFactor;

            transform.localScale = new Vector3(_localScale.x + scale, _localScale.y + scale, _localScale.z + scale);
        }
	}

    public void Activate()
    {
        ActivateHandles();
        DeselectHandles();
        SetType(Type);

        _visible = true;
    }

    public void Deactivate()
    {
        DeactivateHandles();

        _visible = false;
    }

    public void SetTarget(Transform target)
    {
        TargetObject = target;
    }

    public void SetType(GizmoTypes type)
    {
        Type = type;
        _type = Type;

        if (Type == GizmoTypes.Scale)
            transform.SetParent(null);
        else
            transform.SetParent(_parent);

        AxisCenter.SetType(type);
        AxisX.SetType(type);
        AxisY.SetType(type);
        AxisZ.SetType(type);
    }

    public void DeselectHandles()
    {
        AxisCenter.Deselected();
        AxisX.Deselected();
        AxisY.Deselected();
        AxisZ.Deselected();
    }

    private void InitializeHandles()
    {
        AxisCenter.Axis = GizmoAxis.Center;
        AxisCenter.Gizmo = this;

        AxisX.Axis = GizmoAxis.X;
        AxisX.Gizmo = this;

        AxisY.Axis = GizmoAxis.Y;
        AxisY.Gizmo = this;

        AxisZ.Axis = GizmoAxis.Z;
        AxisZ.Gizmo = this;
    }

    private void ActivateHandles()
    {
        AxisCenter.SetActive(true);
        AxisX.SetActive(true);
        AxisY.SetActive(true);
        AxisZ.SetActive(true);
    }

    private void DeactivateHandles()
    {
        AxisCenter.SetActive(false);
        AxisX.SetActive(false);
        AxisY.SetActive(false);
        AxisZ.SetActive(false);
    }
}
